"""H2 Tournament System Main System Classes"""


class Judge():
    """
    Class implementing a judge/referee.
    attributes: first_name - string
                last_name - string
                judge_level - string, techniczny, klubowy, krajowy A,
                              krajowy B, międzynarodowy, światowy
    methods:    get_name_as_dict(self) - returns a dictionary with judge name
                and level
    """
    def __init__(self, first_name, last_name, judge_level):
        """
        Initialize judge.
        """
        self.first_name = first_name
        self.last_name = last_name
        self.level = judge_level

    def __str__(self):
        return(self.first_name + " " + self.last_name + ",  " + self.level)

    def get_name_as_dict(self):
        """Return dictionary with judge data"""
        judge_dict = {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'level': self.level}
        return judge_dict


class Contestant():
    """
    Class modelling contestants
        attributes: first_name - string
                    last_name - string
                    grade - string
                    club_name - string
    """
    def __init__(self, first_name, last_name, grade, club_name):
        """Initialize Contestant with first name, last name, and grade"""
        self.first_name = first_name
        self.last_name = last_name
        self.grade = grade
        self.club_name = club_name

    def __str__(self):
        return(self.first_name + " " + self.last_name + " " + self.club_name)

    def get_name_as_dict(self):
        """Return contestant name as a dictionary"""
        contestant_dict = {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'grade': self.grade,
            'club_name': self.club_name}
        return contestant_dict


class Tatami():
    """
        Class modelling a tatami
        attributes: number - int
                    number_of_referees - int (3,5 or 7)
                    manager - Instance of Judge class
                    referees - list of instances of Judge class
                    kansa - instace of Judge class
        methods:    set_manager(manager) - set tatami manager
                    set_kansa(kansa) - set tatami kansa
                    set_referees(*referees) store list of referees
                    change_manager(new_manager) - change tatami manager
                    change_referee(new_referee) - change one referee
                    change_all_referees(*new_referees) - change all referees
                    change_number_of_referees(new_number)
    """

    def __init__(self, number, number_of_referees):
        """Initialize tatami with number and number of referees"""
        pass

    def set_manager(self, manager):
        """Set tatami manager. Arg: instance of class Judge"""
        self.manager = manager

    def set_kansa(self, kansa):
        """Set tatami kansa. Arg: instance of class Judge"""
        self.kansa = kansa

    def set_referees(self, *referees):
        """Set list of referees"""
        self.referees_list = []

        for referee in referees:
            self.referees.append()

    def change_all_referees(self, *new_referees):
        """
        Change all referees
        TODO: Should the referees list be stored whatsoever?
        
        """
        pass
